<div id="eveque">

  <div class="col1">
    <h1>Festival Poisson-Évêque</h1>
    <h2>du 21 au 30 avril 2017</h2>
    <h2>Le Maga, avenue Jean Volders, 56, 1060 S<sup>t</sup>-Gilles</h2>
    <h2>Bar Waterloo, Chaussée de Waterloo, 217, 1060 S<sup>t</sup>-Gilles</h2>
    <h2>Snack Atlas, Parvis de la Trinité, 6, 1060 S<sup>t</sup>-Gilles</h2>
    <br>
    <p>
      Le festival Poisson-Evêque est une première édition regroupant des artistes de Finlande, de Suisse, de France, de Hollande, de Belgique et de Roumanie ayant une pratique de la sculpture, de la performance et de la ballade. Les actions proposés questionneront la gentrification, les féminismes émergeants, les effets sociaux du capitalisme et les manières de se réapproprier les outils techniques et technologiques contemporains. Les événements du festival se dérouleront au Maga, lieu associatif situé dans le quartier très populaire de Saint-Gilles mais aussi dans des snacks, des bars et dans la ville de Bruxelles, afin de proposer une forme d'art social et utopiste engageant un dialogue avec la population présente.
    </p>
    <br>
    <br>

  </div>
  <div class="col2">
    <h2>P r o g r a m m e</h2>
    <br>

    <h3>Vendredi 21 avril</h3>
    <ul>
      <li>
        <span>18h00</span>
        <span>Jot Fau, Stephane Blumenschein, Xavier Gorgol &amp; Rares Craiut, Irina Favero-Longo</span>
        <span></span><br>
        <span>V e r n i s s a g e&nbsp;&nbsp;&nbsp;d e&nbsp;&nbsp;&nbsp;l ' e x p o s i t i o n</span><br>
        <span>Le Maga</span>
      </li>
      <li>
        <span>19h30</span>
        <span>Irina Favero-Longo et Rébecca Konforti</span>
        <span>Posé entre le coin et la marche</span><br>
        <span>p e r f o r m a n c e</span><br>
        <span>Le Maga</span>
      </li>
      <li>
        <span>20h00</span>
        <span>André D. Chapatte</span>
        <span></span><br>
        <span>p e r f o r m a n c e</span><br>
        <span>Le Maga<br>Snack Atlas</span>
      </li>
    </ul>
    <br>

    <h3>Samedi 22 avril</h3>
    <ul>
      <li>
        <span>19h00</span>
        <span>Rares Craiut &amp; Xavier Gorgol</span>
        <span></span><br/>
        <span>p e r f o r m a n c e&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;c o n v e r s a t i o n&nbsp;&nbsp;&nbsp;p u b l i q u e</span><br/>
        <span>Le Maga</span>
      </li>
      <li>
        <span>20h00</span>
        <span>Sandra Naji &amp; Antoine Loyer</span>
        <span>Rituel des vieilles de Bredene-Aan-Zee</span><br>
        <span>r é c i t&nbsp;&nbsp;&nbsp;m u s i c a l</span><br>
        <span>Le Maga</span>
      </li>
      <li>
        <span>21h00</span>
        <span>Otto Byström</span>
        <span></span><br>
        <span>P e r f o r m a n c e&nbsp;&nbsp;&nbsp;a v e c&nbsp;&nbsp;&nbsp;u n e&nbsp;&nbsp;&nbsp;g u i t a r e&nbsp;&nbsp;&nbsp;é l e c t r i q u e</span><br>
        <span>Bar Waterloo</span>
      </li>
      <li>
        <span>21h30</span>
        <span>André D. Chapatte</span>
        <span>Zachary Airhorn Show</span><br>
        <span>c o n c e r t</span><br>
        <span>Bar Waterloo</span>
      </li>
      <li>
        <span>22h30</span>
        <span>Maxime Lacôme</span>
        <span></span><br>
        <span>Concert</span><br>
        <span>Bar Waterloo</span>
      </li>
    </ul>
  </div>

  <div class="col3">
    <ul>
      <li>
        <span>23h30</span>
        <span>Brique Corps</span>
        <span></span><br>
        <span>M i x&nbsp;&nbsp;&nbsp;d e&nbsp;&nbsp;&nbsp;B r e a k c o r e</span><br>
        <span>Bar Waterloo</span>
      </li>
      <li>
        <span>00h30</span>
        <span>Bettina Kordjani</span>
        <span>Pussies grab back</span><br>
        <span>c h r o n i q u e&nbsp;&nbsp;&nbsp;m us i c a l e&nbsp;&nbsp;&nbsp;d u&nbsp;&nbsp;&nbsp;f é m i n i s m e</span><br>
        <span>Bar Waterloo</span><br>
      </li>
    </ul>
    <br>

    <h3>Dimanche 23 avril</h3>
    <ul>
      <li>
        <span>15h&thinsp;→&thinsp;18h</span>
        <span>Marine Kaiser &amp; Stéphanie Verin</span>
        <span>La ballade de Kaiser et Qasimov (épisode 1)</span><br>
        <span>r e p o r t a g e&nbsp;&nbsp;&nbsp;s i t u é</span><br>
        <span>Départ → Centre Culturel de Uccle</span>
      </li>
      <li>
        <span>00h00&thinsp;→&thinsp;1h30</span>
        <span>Clément Thiry</span>
        <span></span><br>
        <span>V i s i t e&nbsp;&nbsp;&nbsp;d e s&nbsp;&nbsp;&nbsp;c o i n s&nbsp;&nbsp;&nbsp;s o m b r e s&nbsp;d e&nbsp;&nbsp;&nbsp;B r u x e l l e s</span><br>
        <span>lieu de rdv à déterminer</span>
      </li>
    </ul>
    <br>

    <h3>Lundi 24 avril</h3>
    <ul>
      <li>00h → 1h30 > Clément Thiry > Visite des coins sombres de Bruxelles > lieu de rdv à déterminer</li>
    </ul>
    <br>

    <h3>mardi 25 avril</h3>
    <ul>
      <li>00h00 → 1h30 > Clément Thiry > Visite des coins sombres de Bruxelles > lieu de rdv à déterminer</li>
    </ul>
    <br>

    <h3>Mercredi 26 avril</h3>
    <ul>
      <li>19h30 > POP (Potential Office Project) > Balade de bar en bar et lectures > Départ > Maga > Point d’arrivée > Le Midpoint Café</li>
      <li>00h00 → 1h30 > Clément Thiry > Visite des éclairages défectueux de Bruxelles > départ Midpoint Café</li>
    </ul>
  </div>

  <div class="col4">
    <h3>Jeudi 27 avril</h3>
    <ul>
      <li>18h00 > Stéphanie Becquet, Marjolein Guldentops et Romy Vanderveken > Kitchen Session > Chaussée de Forest, 106, Saint-Gilles</li>
    </ul>
    <br>

    <h3>Vendredi 28 avril</h3>
    <ul>
      <li>10h → 13h > Marine Kaiser et Stéphanie Verin > La ballade de Kaiser et Qasimov (épisode 2) > Départ > Jardin du musée Royal de Belgique.</li>
      <li>18h00 > Stéphanie Becquet, Marjolein Guldentops et Romy Vanderveken > Kitchen Session > Chaussée de Forest, 1060, Saint-Gilles</li>
    </ul>
    <br>

    <h3>Dimanche 30 avril</h3>
    <ul>
      <li>Lancement de l’édition du festival Poisson-Evêque > Maga</li>
      <li>19h00 > Irina Favero Longo et Rebecca Konforti > Posé entre le coin et la marche > performance > Maga > 30 mn</li>
    </ul>
    <br>
    <br>

    <div class="crédits">
      <p>
        Un festival organisé par Stéphanie Verin, asssistée d'Émilie Salson
      </p>
      <p>
        Design graphique: Romain Marula &amp; Étienne Ozeray
      </p>
    </div>
  </div>
</div>

<div id="poisson">

  <div class="col1">
    <h1>Festival Poisson-Évêque</h1>
    <h2>du 21 au 30 avril 2017</h2>
    <h2>Le Maga, avenue Jean Volders, 56, 1060 S<sup>t</sup>-Gilles</h2>
    <h2>Bar Waterloo, Chaussée de Waterloo, 217, 1060 S<sup>t</sup>-Gilles</h2>
    <h2>Snack Atlas, Parvis de la Trinité, 6, 1060 S<sup>t</sup>-Gilles</h2>
    <br>
    <p>
      Le festival Poisson-Evêque est une première édition regroupant des artistes de Finlande, de Suisse, de France, de Hollande, de Belgique et de Roumanie ayant une pratique de la sculpture, de la performance et de la ballade. Les actions proposés questionneront la gentrification, les féminismes émergeants, les effets sociaux du capitalisme et les manières de se réapproprier les outils techniques et technologiques contemporains. Les événements du festival se dérouleront au Maga, lieu associatif situé dans le quartier très populaire de Saint-Gilles mais aussi dans des snacks, des bars et dans la ville de Bruxelles, afin de proposer une forme d'art social et utopiste engageant un dialogue avec la population présente.
    </p>
    <br>
    <br>

  </div>
  <div class="col2">
    <h2>Programme</h2>
    <br>

    <h3>Vendredi 21 avril</h3>
    <ul>
      <li>18h00 > Jot Fau, Stephane Blumenschein, Xavier Gorgol et Rares Craiut, Irina Favero-Longo > Vernissage de l'exposition > Maga</li>
      <li>19h30 > Irina Favero-Longo et Rébecca Konforti > Posé entre le coin et la marche > performance > Maga > 30 mn</li>
      <li>20h00 > André D. Chapatte > performance > Maga et Snack Atlas > 40 mn</li>
    </ul>
    <br>

    <h3>Samedi 22 avril</h3>
    <ul>
      <li>19h00 > Rares Craiut et Xavier Gorgol > Transition > performance / conversation publique > Maga > 40 mn</li>
      <li>20h00 > Sandra Naji et Antoine Loyer > Rituel des vieilles de Bredene-Aan-Zee> récit musical > Maga > 20mn</li>
      <li>21h00 > Otto Byström > Performance avec une guitare électrique > Bar Waterloo > 20 mn</li>
      <li>21h30 > André D. Chapatte > Zachary Airhorn Show > concert > Bar Waterloo > 40mn</li>
      <li>22h30 > Maxime Lacôme > Concert > Bar Waterloo > 40 mn</li>
    </ul>
  </div>

  <div class="col3">
    <ul>
      <li>23h30 > Brique Corps > Mix de Breakcore > Bar Waterloo > 40 mn</li>
      <li>00h30 > Bettina Kordjani > Pussies grab back > chronique musicale du féminisme > Bar Waterloo > 1h30</li>
    </ul>
    <br>

    <h3>Dimanche 23 avril</h3>
    <ul>
      <li>15h-18h > Marine Kaiser et Stéphanie Verin > La ballade de Kaiser et Qasimov (épisode 1) > reportage situé > Départ > Centre culturel de Uccle.</li>
      <li>00h00- 1h30 > Clément Thiry > Visite des coins sombres de Bruxelles > lieu de rdv à déterminer</li>
    </ul>
    <br>

    <h3>Lundi 24 avril</h3>
    <ul>
      <li>00h-1h30 > Clément Thiry > Visite des coins sombres de Bruxelles > lieu de rdv à déterminer</li>
    </ul>
    <br>

    <h3>mardi 25 avril</h3>
    <ul>
      <li>00h00-1h30 > Clément Thiry > Visite des coins sombres de Bruxelles > lieu de rdv à déterminer</li>
    </ul>
    <br>

    <h3>Mercredi 26 avril</h3>
    <ul>
      <li>19h30 > POP (Potential Office Project) > Balade de bar en bar et lectures > Départ > Maga > Point d’arrivée > Le Midpoint Café</li>
      <li>00h00- 1h30 > Clément Thiry > Visite des éclairages défectueux de Bruxelles > départ Midpoint Café</li>
    </ul>
  </div>

  <div class="col4">
    <h3>Jeudi 27 avril</h3>
    <ul>
      <li>18h00 > Stéphanie Becquet, Marjolein Guldentops et Romy Vanderveken > Kitchen Session > Chaussée de Forest, 106, Saint-Gilles</li>
    </ul>
    <br>

    <h3>Vendredi 28 avril</h3>
    <ul>
      <li>10h-13h > Marine Kaiser et Stéphanie Verin > La ballade de Kaiser et Qasimov (épisode 2) > Départ > Jardin du musée Royal de Belgique.</li>
      <li>18h00 > Stéphanie Becquet, Marjolein Guldentops et Romy Vanderveken > Kitchen Session > Chaussée de Forest, 1060, Saint-Gilles</li>
    </ul>
    <br>

    <h3>Dimanche 30 avril</h3>
    <ul>
      <li>Lancement de l’édition du festival Poisson-Evêque > Maga</li>
      <li>19h00 > Irina Favero Longo et Rebecca Konforti > Posé entre le coin et la marche > performance > Maga > 30 mn</li>
    </ul>
    <br>
    <br>

    <div class="crédits">
      <p>
        Un festival organisé par Stéphanie Verin, asssistée d'Émilie Salson
      </p>
      <p>
        Design graphique: Romain Marula &amp; Étienne Ozeray
      </p>
    </div>
  </div>
</div>
