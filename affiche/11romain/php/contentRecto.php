

<div class="contentcontent">

	<div class="titre"><sup>Festival</sup><span>Poisson-Évêque</span> ⟶ 21 au 30 avril 2017</div>
<!--
	<div class="date">

	</div> -->


	<div class="people">
		<!-- <p class="curatrice"><b>Curatrice&thinsp;:</b>
			Un festival organisé par Stéphanie Vérin.
		</p> -->
		<ul><li class="list1"><sup>Kitchen</sup><span>Marjolein</span> <span>Guldentops</span></li>
			<li class="list2"><sup>Perfomance</sup><span>Stéphanie</span> <span>Becquet</span></li>
			<li class="list3"><sup>Balade</sup><span>Stephan</span> <span>Blumenschein</span></li>
			<li class="list4"><sup>Performance</sup><span>Otto</span> <span>Byström</span></li>
			<li class="list5"><sup>Concert</sup><span>André</span> <span>D.Chapatte</span></li>
			<li class="list6"><sup>Concert</sup><span>Brique</span> <span>Corps</span></li>
			<li class="list7"><sup>Performance</sup><span>Rares</span> <span>Craiut</span></li>
			<li class="list8"><sup>Vernissage</sup><span>Jot</span> <span>Fau</span></li>
			<li class="list9"><sup>Performance</sup><span>Irina</span> <span>Favero-Longo</span></li>
			<li class="list10"><sup>Performance</sup><span>Xavier</span> <span>Gorgol</span></li>
			<li class="list11"><sup>Balade</sup><span>Marine</span> <span>Kaiser</span></li>
			<li class="list12"><sup>Concert</sup><span>Maxime</span> <span>Lacôme</span></li>
			<li class="list13"><sup>Récit</sup><span>Antoine</span> <span>Loyer</span></li>
			<li class="list14"><sup>Récit</sup><span>Sandra</span> <span>Naji</span></li>
			<li class="list15"><sup>Balade</sup><span>Clément</span> <span>Thiry</span></li>
			<li class="list16"><sup>Performance</sup><span>Rebecca</span> <span>Konforti</span></li>
			<li class="list17" style="text-align:left;"><sup>Balade</sup><span>Potential Office&thinsp;Project</span></span></li>
				<li class="list19"><sup>Balade</sup><span>Stéphanie</span> <span>Verin</span></li>
			<li class="list18"><sup>Chronique</sup><span>Bettina</span> <span>Kordjani</span></li>
			<li class="list20"><sup>Kitchen</sup><span>Romy</span> <span>Vanderveken</span></li>
		</ul>
	</div>

	<p class="lieu">
		<span>Le Maga</span>
		<span>Avenue Jean Volders, 56</span><br>
		</p>
	<p class="lieu">
		<span> Bar Waterloo</span>
		<span>Chaussée de Waterloo, 217</span><br>
	
	</p>
	<p class="lieu">
		<span> Snack ATLAS</span>
		<span>Parvis de la Trinité, 6</span><br>
	
	</p>

<!--	<?php include('texte-steph.php') ?>-->

</div>

<div class="marge margeHoriz margeTop"></div>
<div class="marge margeHoriz margeBottom"></div>
<div class="marge margeVert margeLeft"></div>
<div class="marge margeVert margeRight"></div>

<!-- <div id="gridVisible">
	<div class="rows">
		<div class="line horizontal row"></div>
	</div>
	<div class="cols">
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
	</div>
</div> -->

<!-- <img class="bgOr" src="img/Hobos2.jpg" alt="" /> -->

<div class="rectangle"></div>
