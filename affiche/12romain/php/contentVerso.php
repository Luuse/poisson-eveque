<div id="eveque">
  <h1>Programme</h1>

  <h3>avec:
  <ul>
    <li>Marine Kaiser & Stéphanie Verin, Titres, Date, durée, lieux de vos performances</li>
    <li>André D. Chapatte, Titres, Date, durée, lieux de vos performances</li>
    <li>Rares Craiut & Xavier Gorgol, Titres, Date, durée, lieux de vos performances</li>
    <li>Jot Fau, Titres, Date, durée, lieux de vos performances</li>
    <li>Stephan Blumenschein,Titres, Date, durée, lieux de vos performances</li>
    <li>Irina Favero-Longo & Rebecca Konforti, Titres, Date, durée, lieux de vos performances</li>
    <li>Sandra Naji & Antoine Loyer, Titres, Date, durée, lieux de vos performances</li>
    <li>Otto Byström, Titres, Date, durée, lieux de vos performances</li>
    <li>Maxime Lacôme, Titres, Date, durée, lieux de vos performances</li>
    <li>Clément Thiry, Titres, Date, durée, lieux de vos performances</li>
    <li>Stéphanie Becquet, Titres, Date, durée, lieux de vos performances</li>
    <li>Marjolein, Titres, Date, durée, lieux de vos performances</li>
    <li>Romy, Titres, Date, durée, lieux de vos performances</li>
    <li>Brique Corps, Titres, Date, durée, lieux de vos performances</li>
    <li>POP Titres, Date, durée, lieux de vos performances</li>
  </ul>
</h3>

  <p>
    Le festival Poisson-Evêque est une première édition regroupant des artistes de Finlande, de Suisse, de France, de Belgique
    et de Roumanie ayant une pratique de la sculpture, de la performance et de la ballade. Les actions proposés questionneront
    la gentrification, les féminismes émergeants, les effets sociaux du capitalisme et les manières de se réapproprier les outils
    techniques et technologiques contemporains. Les événements du festival se dérouleront au Maga, lieu associatif situé dans le quartier
    très populaire de Saint-Gilles mais aussi dans des snacks, des bars et dans la ville de Bruxelles, afin de proposer une forme d'art social
    et utopiste engageant un dialogue avec la population présente.
  </p>

  <div class="signe-eveque"> ♛ </div>

  <ul>
    <li>Vendredi 21 avril 18h00 Ouverture de l’exposition (titre ? ) avec des sculptures et installations de Jot Fau,
    Stephane Blumenschein, Stéphanie Becquet, Xavier Gorgol et Rares Craiut, Irina Favero Longo. </li>
    <li>19h00 Sandra Naji et Antoine Loyer > (titre) Spectacle musical avec projection > 20mn</li>
    <li> 20h00 André D. Chapatte > (titre) performance, départ du maga et dans un snack proche > 40 mn (BAR)</li>
    <li>Samedi 22 avril 19h Xavier Gorgol et Rares Craiut > « Transition » conversation publique > 40 mn</li>
    <li>20h Otto Byström > Performance avec une guitare électrique > (au Queens) > 20 mn</li>
    <li>21h (Maxime Lacôme et/ou Zachary Airhorn) > Concert > (au Queens) > (durée) (BAR)</li>
    <li>Dimanche 23 avril 15h-18h : La ballade de Kaiser et Qasimov (épisode 1) > Départ Centre culturel de Uccle
    <li>La nuit, Clément T, visite des lieux non éclairés de Bruxelles > (lieux de Rdv) (Durée)</li>
    <li>Lun 24 > 00h à 1h30 >Visite des coins sombres de Bruxelles, Clément Thiry, mard 25> 00h à 1h30 > Visite des coins sombres de Bruxelles, Clément Thiry</li>
    <li>mercredi 25 avril > 19h30 jusque ... ?> rdv au maga> POP > Ballade de bar en bar et lectures > Le Cobra Jaune > Le Santana > Point d’arrivée > Le Midpoint</li>
    <li> 00h>1h30 > Visite des coins sombres de Bruxelles > Clément Thiry</li>
    <li>Jeudi 26 avril > Stéphanie Becquet/ Romy/ Marjolein> Session Cuisine > Kitchen Session > durée ? > adresse.</li>
    <li>Vendredi 27 avril > 10h-13h > La ballade de Kaiser et Qasimov (épisode 2) Départ > Jardin du musée Royal de Belgique.</li>
    <li>Samedi 29 avril > 19h Performance Irina Favero Longo/ Rebecca Konforti> (titre) (durée) au Maga Lancement de l’édition du festival Poisson-Evêque au Maga ( titre ?) (BAR)</li>
  </ul>
</div>

<div id="poisson">
  <h1>Program</h1>

<h3>with:
  <ul>
    <li>Marine Kaiser & Stéphanie Verin, Titres, Date, durée, lieux de vos performances</li>
    <li>André D. Chapatte, Titres, Date, durée, lieux de vos performances</li>
    <li>Rares Craiut & Xavier Gorgol, Titres, Date, durée, lieux de vos performances</li>
    <li>Jot Fau, Titres, Date, durée, lieux de vos performances</li>
    <li>Stephan Blumenschein,Titres, Date, durée, lieux de vos performances</li>
    <li>Irina Favero-Longo & Rebecca Konforti, Titres, Date, durée, lieux de vos performances</li>
    <li>Sandra Naji & Antoine Loyer, Titres, Date, durée, lieux de vos performances</li>
    <li>Otto Byström, Titres, Date, durée, lieux de vos performances</li>
    <li>Maxime Lacôme, Titres, Date, durée, lieux de vos performances</li>
    <li>Clément Thiry, Titres, Date, durée, lieux de vos performances</li>
    <li>Stéphanie Becquet, Titres, Date, durée, lieux de vos performances</li>
    <li>Marjolein, Titres, Date, durée, lieux de vos performances</li>
    <li>Romy, Titres, Date, durée, lieux de vos performances</li>
    <li>Brique Corps, Titres, Date, durée, lieux de vos performances</li>
    <li>POP Titres, Date, durée, lieux de vos performances</li>
  </ul>
</h3>


    <p>“Poisson-evêque” is the first edition of a festival gathering artists coming from Finland, Swiss, France,
      Belgium and Romania, who practicing sculpture, performance and musical ballads. Action proposed
      will questioning gentrification, feminism, social effects of capitalism and the reappropriation of technical
      and technological tools.Events during the festival will take place at Maga, an associatif place in the very popular neighbourhood
      of Saint Gilles but also in bars, snack, and in the city of Brussels, to offer a kind of social and utopist art and by doing so,
      bring a dialogue with the locals.
  </p>

  <div id="signe-poisson"> ☠ </div>
<ul>
<li>Vendredi 21 avril 18h00 Ouverture de l’exposition (titre ? ) avec des sculptures et installations de Jot Fau,
Stephane Blumenschein, Stéphanie Becquet, Xavier Gorgol et Rares Craiut, Irina Favero Longo. </li>
<li>19h00 Sandra Naji et Antoine Loyer > (titre) Spectacle musical avec projection > 20mn</li>
<li> 20h00 André D. Chapatte > (titre) performance, départ du maga et dans un snack proche > 40 mn (BAR)</li>
<li>Samedi 22 avril 19h Xavier Gorgol et Rares Craiut > « Transition » conversation publique > 40 mn</li>
<li>20h Otto Byström > Performance avec une guitare électrique > (au Queens) > 20 mn</li>
<li>21h (Maxime Lacôme et/ou Zachary Airhorn) > Concert > (au Queens) > (durée) (BAR)</li>
<li>Dimanche 23 avril 15h-18h : La ballade de Kaiser et Qasimov (épisode 1) > Départ Centre culturel de Uccle
<li>La nuit, Clément T, visite des lieux non éclairés de Bruxelles > (lieux de Rdv) (Durée)</li>
<li>Lun 24 > 00h à 1h30 >Visite des coins sombres de Bruxelles, Clément Thiry, mard 25> 00h à 1h30 > Visite des coins sombres de Bruxelles, Clément Thiry</li>
<li>mercredi 25 avril > 19h30 jusque ... ?> rdv au maga> POP > Ballade de bar en bar et lectures > Le Cobra Jaune > Le Santana > Point d’arrivée > Le Midpoint</li>
<li> 00h>1h30 > Visite des coins sombres de Bruxelles > Clément Thiry</li>
<li>Jeudi 26 avril > Stéphanie Becquet/ Romy/ Marjolein> Session Cuisine > Kitchen Session > durée ? > adresse.</li>
<li>Vendredi 27 avril > 10h-13h > La ballade de Kaiser et Qasimov (épisode 2) Départ > Jardin du musée Royal de Belgique.</li>
<li>Samedi 29 avril > 19h Performance Irina Favero Longo/ Rebecca Konforti> (titre) (durée) au Maga Lancement de l’édition du festival Poisson-Evêque au Maga ( titre ?) (BAR)</li>
</ul></div>
</div>
