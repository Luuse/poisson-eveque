

<div class="contentcontent">

	<div class="titre"><span>Festival Poisson Évêque</span></br> → 21 au 30 avril 2017</div>
<!--
	<div class="date">

	</div> -->


	<div class="people">
		<!-- <p class="curatrice"><b>Curatrice&thinsp;:</b>
			Un festival organisé par Stéphanie Vérin.
		</p> -->
		<ul><li class="list1"><span>Stéphanie</span> <sup>P e r f o m a n c e</sup> <span>Becquet</span></li>
			<li class="list2"><span>Stephan</span> <sup>B a l a d e</sup> <span>Blumenschein</span></li>
			<li class="list3"><span>Otto</span> <sup>P e r f o r m a n c e</sup> <span>Byström</span></li>
			<li class="list4"><span>André</span> <sup>C o n c e r t</sup> <span>D.Chapatte</span></li>
			<li class="list5"><span>Brique</span> <sup>C o n c e r t</sup> <span>Corps</span></li>
			<li class="list6"><span>Rares</span> <sup>P e r f o r m a n c e</sup> <span>Craiut</span></li>
			<li class="list7"><span>Jot</span> <sup>V e r n i s s a g e</sup> <span>Fau</span></li>
			<li class="list8"><span>Irina</span> <sup>P e r f o r m a n c e</sup> <span>Favero-Longo</span></li>
			<li class="list9"><span>Xavier</span>  <sup>P e r f o r m a n c e</sup> <span>Gorgol</span></li>
			<li class="list10"><span>Marine</span> <sup>B a l a d e</sup> <span>Kaiser</span></li>
			<li class="list11"><span>Marjolein</span> <sup>K i t c h e n - s e s s i o n</sup> <span>Guldentops</span></li>
			<li class="list12"><span>Maxime</span> <sup>C o n c e r t</sup> <span>Lacôme</span></li>
			<li class="list13"><span>Antoine</span> <sup>R é c i t - m u s i c a l</sup> <span>Loyer</span></li>
			<li class="list14"><span>Sandra</span> <sup>R é c i t - m u s i c a l</sup> <span>Naji</span></li>
			<li class="list15"><span>Clément</span> <sup>B a l a d e</sup> <span>Thiry</span></li>
			<li class="list16"><span>Rebecca</span> <sup>P e r f o r m a n c e</sup> <span>Konforti</span></li>
			<li class="list17"><span>Bettina</span> <sup>C h r o n i q u e</sup> <span>Kordjani</span></li>
			<li class="list18" style="text-align:left;"><span>Potential<sup> B a l a d e </sup>  Office&thinsp;Project</span></span></li>
			<li class="list19"><span>Romy</span> <sup>K i t c h e n - s e s s i o n</sup> <span>Vanderveken</span></li>	
			<li class="list20"><span>Stéphanie</span> <sup>B a l a d e</sup> <span>Verin</span></li>
		</ul>
	</div>

	<p class="lieu">
		<span>Le Maga</span>
		<span>Avenue Jean Volders, 56</span><br>
		</p>
	<p class="lieu">
		<span> Bar Waterloo</span>
		<span>Chaussée de Waterloo, 217</span><br>
	
	</p>
	<p class="lieu">
		<span> Snack Atlas</span>
		<span>Parvis de la Trinité, 6</span><br>
	
	</p>

<!--	<?php include('texte-steph.php') ?>-->

</div>

<div class="marge margeHoriz margeTop"></div>
<div class="marge margeHoriz margeBottom"></div>
<div class="marge margeVert margeLeft"></div>
<div class="marge margeVert margeRight"></div>

<!-- <div id="gridVisible">
	<div class="rows">
		<div class="line horizontal row"></div>
	</div>
	<div class="cols">
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
	</div>
</div> -->

<!-- <img class="bgOr" src="img/Hobos2.jpg" alt="" /> -->

<div class="rectangle"></div>
