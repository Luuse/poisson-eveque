<div class="contentcontent">

	<div class="titre"><span>Poisson-Évêque</span> <span>21 → 30 avril 2017</span></div>
<!--
	<div class="date">

	</div> -->


	<div class="people">
		<!-- <p class="curatrice"><b>Curatrice&thinsp;:</b>
			Un festival organisé par Stéphanie Vérin.
		</p> -->
		<ul>
			<li class="list1"><span>Stéphanie</span> <span>Stéphanie</span></li>
			<li class="list2"><span>Stephan</span> <span>Stephan</span></li>
			<li class="list3"><span>Otto</span> <span>Otto</span></li>
			<li class="list4"><span>André</span> <span>André</span></li>
			<li class="list5"><span>Brique</span> <span>Brique</span></li>
			<li class="list6"><span>Rares</span> <span>Rares</span></li>
			<li class="list7"><span>Jot</span> <span>Jot</span></li>
			<li class="list8"><span>Irina</span> <span>Irina</span></li>
			<li class="list9"><span>Xavier</span> <span>Xavier</span></li>
			<li class="list10"><span>Marjolein</span> <span>Marjolein</span></li>
			<li class="list11"><span>Marine</span> <span>Marine</span></li>
			<li class="list12"><span>Maxime</span> <span>Maxime</span></li>
			<li class="list13"><span>Antoine</span> <span>Antoine</span></li>
			<li class="list14"><span>Sandra</span> <span>Sandra</span></li>
			<li class="list15"><span>Clément</span> <span>Clément</span></li>
			<li class="list16"><span>Rebecca</span> <span>Rebecca</span></li>
			<li class="list17"><span>Bettina</span> <span>Bettina</span></li>
			<li class="list18" style="text-align:left;"><span>Potential </span><span>Potential</span></li>
			<li class="list19"><span>Romy</span> <span>Romy</span></li>
			<li class="list20"><span>Stéphanie</span> <span>Stéphanie</span></li>
		</ul>
	</div>

	<p class="lieu">
		<span>Le Maga</span>
		<span>Avenue Jean Volders, 56</span><br>
		<span>1060 Saint-Gilles Belgique</span>
	</p>

</div>

<div class="contentcontentVerso1">

	<div class="titre"><span>Poisson-Évêque</span> <span>21 → 30 avril 2017</span></div>
<!--
	<div class="date">

	</div> -->


	<div class="people">
		<!-- <p class="curatrice"><b>Curatrice&thinsp;:</b>
			Un festival organisé par Stéphanie Vérin.
		</p> -->
		<ul>
			<li class="list1"><span>Becquet</span> <span>Becquet</span></li>
			<li class="list2"><span>Blumenschein</span> <span>Blumenschein</span></li>
			<li class="list3"><span>Byström</span> <span>Byström</span></li>
			<li class="list4"><span>D.Chapatte</span> <span>D.Chapatte</span></li>
			<li class="list5"><span>Corps</span> <span>Corps</span></li>
			<li class="list6"><span>Craiut</span> <span>Craiut</span></li>
			<li class="list7"><span>Fau</span> <span>Fau</span></li>
			<li class="list8"><span>Favero-Longo</span> <span>Favero-Longo</span></li>
			<li class="list9"><span>Gorgol</span> <span>Gorgol</span></li>
			<li class="list10"><span>Guldentops</span> <span>Guldentops</span></li>
			<li class="list11"><span>Kaiser</span> <span>Kaiser</span></li>
			<li class="list12"><span>Lacôme</span> <span>Lacôme</span></li>
			<li class="list13"><span>Loyer</span> <span>Loyer</span></li>
			<li class="list14"><span>Naji</span> <span>Naji</span></li>
			<li class="list15"><span>Thiry</span> <span>Thiry</span></li>
			<li class="list16"><span>Konforti</span> <span>Konforti</span></li>
			<li class="list17"><span>Kordjani</span> <span>Kordjani</span></li>
			<li class="list18" style="text-align:left;"><span>Office Project</span> <span>Office Project</span></li>
			<li class="list19"><span>Vanderveken</span> <span>Vanderveken</span></li>
			<li class="list20"><span>Verin</span> <span>Verin</span></li>
		</ul>
	</div>

	<p class="lieu">
		<span>Le Maga</span>
		<span>Avenue Jean Volders, 56</span><br>
		<span>1060 Saint-Gilles Belgique</span>
	</p>

</div>

<div class="marge margeHoriz margeTop"></div>
<div class="marge margeHoriz margeBottom"></div>
<div class="marge margeVert margeLeft"></div>
<div class="marge margeVert margeRight"></div>

<!-- <div id="gridVisible">
	<div class="rows">
		<div class="line horizontal row"></div>
	</div>
	<div class="cols">
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
		<div class="line vertical col"></div>
	</div>
</div> -->

<!-- <img class="bgOr" src="img/Hobos2.jpg" alt="" /> -->

<div class="rectangle"></div>
