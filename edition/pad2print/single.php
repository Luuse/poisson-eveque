<?php
  include('include/header.php');
  include('include/variables.php');
  $nom = $_GET['slug'];
  // $pad_html = $padUrl.$nom.'_html';
  // $pad_css = $padUrl.$nom.'_css';
?>

  <div id="pageGauche" class="single wrap">
    <div class="theGrid cols">
      <?php for($i = 0; $i < 27; $i++){
        echo '<div class="col '.$i.'"></div>';
      } ?>
    </div>
    <div class="theGrid rows">
      <?php for($i = 0; $i < 48; $i++){
        echo '<div class="row '.$i.'"></div>';
      } ?>
    </div>
    <iframe class="pages gauche" id="pageG" src="pages.php?slug=<?= $nom ?>&amp;page=gauche" allowtransparency="true"></iframe>
  </div>
  <div id="pageDroite" class="single wrap">
    <div class="theGrid">
      <?php for($i = 0; $i < 27; $i++){
        echo '<div class="col '.$i.'"></div>';
      } ?>
    </div>
    <div class="theGrid rows">
      <?php for($i = 0; $i < 48; $i++){
        echo '<div class="row '.$i.'"></div>';
      } ?>
    </div>
    <iframe class="pages droite" id="pageD"  src="pages.php?slug=<?= $nom ?>&amp;page=droite" allowtransparency="true"></iframe>
  </div>

  <div id="pads">
    <iframe class="html 1" src="<?= $padHtml1 ?>"></iframe>
    <iframe class="html 2" src="<?= $padHtml2 ?>"></iframe>
    <iframe class="html 3" src="<?= $padHtml3 ?>"></iframe>
    <iframe class="html 4" src="<?= $padHtml4 ?>"></iframe>
    <iframe class="css" src="<?= $padCss ?>"></iframe>
  </div>

<?php
  include('include/nav.php');
  include('include/footer.php');
?>
