function reloadPreview(button, pageG, pageD){

  var srcG = pageG.attr('src');
  var srcD = pageD.attr('src');

  $(document).keypress(function(e) {
    if ( e.which == 114 ) {
      var number = 1 + Math.floor(Math.random() * 10000);
      pageG.removeAttr('src');
      pageD.removeAttr('src');
      pageG.attr('src', srcG+'&rand='+number);
      pageD.attr('src', srcD+'&rand='+number);
    }
  })

  button.click(function(){
    var number = 1 + Math.floor(Math.random() * 10000);
    pageG.removeAttr('src');
    pageD.removeAttr('src');
    pageG.attr('src', srcG+'&rand='+number);
    pageD.attr('src', srcD+'&rand='+number);
  })
}


function showPadsAndGrid(button, el, start){
  if(start == true){
    el.hide();
  }
  button.click(function(){
    if($(this).children().html() == 'OFF'){
      $(this).children().html('ON');
      el.show();
    } else {
      $(this).children().html('OFF');
      el.hide();
    }
  })
}


function zoom(sizer, pageG, pageD, gridG, gridD){
  var marginLeft;
  sizer.children('[type=range]').on('input change', function() {
    var value = $(this).val();
    value = value/100;
    pageG.css({'transform': 'scale('+value+')'});
    pageD.css({'transform': 'scale('+value+')'});
    gridG.css({'transform': 'scale('+value+')'});
    gridD.css({'transform': 'scale('+value+')'});
    marginLeft = pageG.width()*(value);
    pageG.parent().width(marginLeft);
    pageD.parent().width(marginLeft);
    var totalW = pageG.parent().width()+pageD.parent().width();
    var windowW = $(window).width();
    if(totalW >= windowW){
      $('body').width(totalW+200);
    } else{
      $('body').width(windowW);
    }
  })
}

function zoomHome(sizer, double, pageG, pageD){
  var doubleW = double.width();
  var marginLeft;
  sizer.children('[type=range]').on('input change', function(){

    var value = $(this).val();
    value1 = value*7.37;
    console.log(value1);
    value2 = value/200;

    double.width(value1);
    double.height(value1/1.333333333);

    pageG.css({
      'transform': 'scale('+value2+')'
    })
    pageD.css({
      'transform': 'scale('+value2+')'
    })

    marginLeft = pageG.width()*(value2);
    pageG.parent().width(marginLeft);
    pageD.parent().width(marginLeft);
    pageG.parent().height(marginLeft * 1.333333333);
    pageD.parent().height(marginLeft * 1.333333333);

    var margin = double.width();
  })

}

function showPadContent(padsParts, pads){
  pads.children('.html').hide();
  pads.children('.html').eq(0).show();
  padsParts.click(function(){
    var theClass = $(this).attr('class').split(' ')[1];
    padsParts.removeClass('active');
    $(this).addClass('active');
    pads.children('.html').hide();
    pads.children('.'+theClass).show();
  })
}
