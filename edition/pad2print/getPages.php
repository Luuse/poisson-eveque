<style media="screen, print">

  html{
    width: 196mm;
  }

  body{
    margin: 0;
    padding: 0;
  }

  @page{
    size: 196mm 286mm;
    margin: 0;
    padding: 0;
  }

  iframe,
  .pages{
    width: 18.6cm;
    height: 27.6cm;
    /*padding-top: 2mm;*/
    /*margin-top: 8mm;*/
    /*margin-top: 1.1cm;*/
    /*margin-left: 1.5cm;*/
    /*border: 1px solid black;*/
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
  }

  iframe{
    margin-top: 5mm;
    margin-left: 5mm;
  }

  .pages:nth-of-type(even) iframe{
    /*margin-right: 1.5cm;*/
  }

  .pages:nth-of-type(odd) iframe{
    /*margin-left: 1.5cm;*/
  }

  .pageG,
  .pageD{
    -webkit-page-break-after: always;
    page-break-after: always;

  }

  .pageG:nth-of-type(1){
  }

  .cropTop,
  .cropBottom,
  .cropLeft,
  .cropRight{
    position: absolute;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
  }

  .cropTop,
  .cropBottom{
    width: 180mm;
    height: 6mm;
    padding: 0 0.125pt;
    border-left: 0.25pt solid black;
    border-right: 0.25pt solid black;
    margin-left: 8mm;
  }

  .cropTop{
    margin-top: 0mm;
  }

  .cropBottom{
    margin-top: -1mm;
  }

  .cropLeft,
  .cropRight{
    height: 270mm;
    width: 6mm;
    padding: 0.125pt 0pt;
    border-top: 0.25pt solid black;
    border-bottom: 0.25pt solid black;
    margin-top: 8mm;
  }

  .cropLeft{
    left: 0;
  }

  .cropRight{
    right: 0;
  }


</style>
<style media="print">

  iframe{
    border: none;
  }

</style>

<?php

  include('include/variables.php');
  // $setup = file_get_contents($padExport1.'setup'.$padExport2);
  // $setup = 'abcd';
  $pregEnter = array();
  $pregExit = array();
  $set_projet = array();

  // $pregEnter[0] = '/\n/';
  // $pregExit[0] = '~~';

  // $sep = preg_replace($pregEnter, $pregExit, $setup);
  $set_projets = preg_split('/~~/', $setup);
  $set_projets = array_filter($set_projets);

  $set_name = array();
  $set_slug = array();
  $set_projetName = array();

  foreach($set_projets as $set_projet){
	 $explo = explode(' / ', $set_projet);
	 array_push($set_name, $explo[0]);
	 array_push($set_slug, $explo[1]);
	 array_push($set_projetName, $explo[2]);
  }



  $i=0;
  foreach($set_slug as $slug){

  $rand = rand(0, 10000);


  $pad_html = $padUrl.$slug.'_html';
  $pad_css = $padUrl.$slug.'_css';
  $pagi_odd = ($i+1 ) *2;
  $pagi_even = $pagi_odd + 1;

?>

<div class="pageG pages">
  <div class="cropTop"></div>
  <div class="cropLeft"></div>
  <div class="cropRight"></div>
  <iframe class="page gauche" src="pages.php?slug=<?= $slug ?>&amp;reload=off&amp;page=gauche<?php echo '&amp;rand='.$rand; ?>"></iframe>
  <div class="cropBottom"></div>
</div>

<div class="pageD pages">
  <div class="cropTop"></div>
  <div class="cropLeft"></div>
  <div class="cropRight"></div>
  <iframe class="page droite" src="pages.php?slug=<?= $slug ?>&amp;reload=off&amp;page=droite<?php echo '&amp;rand='.$rand; ?>"></iframe>
  <div class="cropBottom"></div>
</div>

<?php
  $i++;
  }
?>
