  <?php
    include('include/variables.php');
    $page       = true;
    $nom        = $_GET['slug'];
    $pageSide   = $_GET['page'];
    $css        = file_get_contents($padCss.'/export/txt');

    $pageHtml1   = file_get_contents($padHtml1.'/export/txt');
    $pageHtml2   = file_get_contents($padHtml2.'/export/txt');
    $pageHtml3   = file_get_contents($padHtml3.'/export/txt');
    $pageHtml4   = file_get_contents($padHtml4.'/export/txt');

    $pages1 = explode('<!-- PAGE -->', $pageHtml1);
    $pages2 = explode('<!-- PAGE -->', $pageHtml2);
    $pages3 = explode('<!-- PAGE -->', $pageHtml3);
    $pages4 = explode('<!-- PAGE -->', $pageHtml4);

    include('include/header.php');

    foreach($pages1 as $page){
      preg_match('/<!-- \[(.*?)\] -->/s', $page, $thePage);
      if($thePage[1] == $nom){
        $result = explode('<!-- BREAK -->', $page);
        if ($pageSide == 'gauche'){
          $result1   = $result[0];
        } else if ($pageSide == 'droite'){
          $result1   = $result[1];
        }
      }
    }

    foreach($pages2 as $page){
      preg_match('/<!-- \[(.*?)\] -->/s', $page, $thePage);
      if($thePage[1] == $nom){
        $result = explode('<!-- BREAK -->', $page);
        if ($pageSide == 'gauche'){
          $result2   = $result[0];
        } else if ($pageSide == 'droite'){
          $result2   = $result[1];
        }
      }
    }

    foreach($pages3 as $page){
      preg_match('/<!-- \[(.*?)\] -->/s', $page, $thePage);
      if($thePage[1] == $nom){
        $result = explode('<!-- BREAK -->', $page);
        if ($pageSide == 'gauche'){
          $result3   = $result[0];
        } else if ($pageSide == 'droite'){
          $result3   = $result[1];
        }
      }
    }

    foreach($pages4 as $page){
      preg_match('/<!-- \[(.*?)\] -->/s', $page, $thePage);
      if($thePage[1] == $nom){
        $result = explode('<!-- BREAK -->', $page);
        if ($pageSide == 'gauche'){
          $result4   = $result[0];
        } else if ($pageSide == 'droite'){
          $result4   = $result[1];
        }
      }
    }
  ?>

    <style>
      @media print, screen {
        <?= $css ?>
      }
    </style>
    <?php
    	if ($pageSide == 'gauche') {
        echo '<div class="partie conversation gauche">';
    	} else if ($pageSide == 'droite'){
        echo '<div class="partie conversation droite">';
      }
      echo $result1;
      echo '</div>';

    	if ($pageSide == 'gauche') {
        echo '<div class="partie documentation gauche">';
    	} else if ($pageSide == 'droite'){
        echo '<div class="partie documentation droite">';
      }
      echo $result2;
      echo '</div>';

    	if ($pageSide == 'gauche') {
        echo '<div class="partie restitution gauche">';
    	} else if ($pageSide == 'droite'){
        echo '<div class="partie restitution droite">';
      }
      echo $result3;
      echo '</div>';

    	if ($pageSide == 'gauche') {
        echo '<div class="partie edito gauche">';
    	} else if ($pageSide == 'droite'){
        echo '<div class="partie edito droite">';
      }
      echo $result4;
      echo '</div>';
    ?>
  </body>
</html>
